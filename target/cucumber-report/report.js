$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("US01 - Settings.feature");
formatter.feature({
  "line": 2,
  "name": "US01 - Settings",
  "description": "",
  "id": "us01---settings",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@US01"
    }
  ]
});
formatter.scenario({
  "line": 5,
  "name": "TC01 - Edit SIM Nickname",
  "description": "",
  "id": "us01---settings;tc01---edit-sim-nickname",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@US01_TC01"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "I navigate to \"https://accounts.amaysim.com.au/identity/login\"",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I type username as \"0468827174\"",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I type password as \"theHoff34\"",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "I click on Login button",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I click on Close welcome modal",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "I should be at Dashboards",
  "keyword": "Then "
});
formatter.step({
  "line": 13,
  "name": "I click on Settings in main menu",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "I wait site to load",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "I should be at Settings page",
  "keyword": "Then "
});
formatter.step({
  "line": 16,
  "name": "I click on Edit Sim Nickname",
  "keyword": "When "
});
formatter.step({
  "line": 17,
  "name": "I type SIM Nickname as \"Test 008\"",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "I click on Submit button",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I should see SIM Nickname as \"Test 008\"",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "https://accounts.amaysim.com.au/identity/login",
      "offset": 15
    }
  ],
  "location": "GenericSteps.i_navigate_to_something(String)"
});
formatter.result({
  "duration": 12443309646,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0468827174",
      "offset": 20
    }
  ],
  "location": "LoginPageSteps.i_type_username_as_something(String)"
});
formatter.result({
  "duration": 224708453,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "theHoff34",
      "offset": 20
    }
  ],
  "location": "LoginPageSteps.i_type_password_as_something(String)"
});
formatter.result({
  "duration": 132680058,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageSteps.i_click_on_login_button()"
});
formatter.result({
  "duration": 61970763286,
  "status": "passed"
});
formatter.match({
  "location": "DashboardsSteps.i_click_on_close_welcom_modal()"
});
formatter.result({
  "duration": 55394462,
  "status": "passed"
});
formatter.match({
  "location": "DashboardsSteps.i_should_be_at_dashboards()"
});
formatter.result({
  "duration": 116148251,
  "status": "passed"
});
formatter.match({
  "location": "DashboardsSteps.i_click_on_settings_in_main_menu()"
});
formatter.result({
  "duration": 71033689,
  "status": "passed"
});
formatter.match({
  "location": "GenericSteps.i_wait_site_to_load()"
});
formatter.result({
  "duration": 7126268110,
  "status": "passed"
});
formatter.match({
  "location": "SettingsPageSteps.i_should_be_at_settings_page()"
});
formatter.result({
  "duration": 110619902,
  "status": "passed"
});
formatter.match({
  "location": "SettingsPageSteps.i_click_on_edit_sim_nickname()"
});
formatter.result({
  "duration": 64420004,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Test 008",
      "offset": 24
    }
  ],
  "location": "SettingsPageSteps.i_type_sim_nickname_as_something(String)"
});
formatter.result({
  "duration": 3210624744,
  "status": "passed"
});
formatter.match({
  "location": "GenericSteps.i_click_on_submit_button()"
});
formatter.result({
  "duration": 72526988,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Test 008",
      "offset": 30
    }
  ],
  "location": "SettingsPageSteps.i_should_see_sim_nickname_as_something(String)"
});
formatter.result({
  "duration": 3848622275,
  "status": "passed"
});
formatter.write("Current Page URL is https://www.amaysim.com.au/my-account/my-amaysim/settings");
formatter.embedding("image/png", "aee2eaa5-e3b8-4735-83dc-5948180a0aec.png");
formatter.scenario({
  "line": 22,
  "name": "TC02 - Disable Call forwarding",
  "description": "",
  "id": "us01---settings;tc02---disable-call-forwarding",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 21,
      "name": "@US01_TC02"
    }
  ]
});
formatter.step({
  "line": 24,
  "name": "I navigate to \"https://accounts.amaysim.com.au/identity/login\"",
  "keyword": "Given "
});
formatter.step({
  "line": 25,
  "name": "I type username as \"0468827174\"",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "I type password as \"theHoff34\"",
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "I click on Login button",
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "I click on Close welcome modal",
  "keyword": "When "
});
formatter.step({
  "line": 29,
  "name": "I should be at Dashboards",
  "keyword": "Then "
});
formatter.step({
  "line": 30,
  "name": "I click on Settings in main menu",
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "I wait site to load",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "I should be at Settings page",
  "keyword": "Then "
});
formatter.step({
  "line": 33,
  "name": "I click on Edit Call forwarding",
  "keyword": "When "
});
formatter.step({
  "line": 34,
  "name": "I wait site to load",
  "keyword": "When "
});
formatter.step({
  "line": 35,
  "name": "I click on Confirm button Modal",
  "keyword": "When "
});
formatter.step({
  "line": 36,
  "name": "I click on No radio button Call forwarding",
  "keyword": "When "
});
formatter.step({
  "line": 37,
  "name": "I click on Submit button",
  "keyword": "When "
});
formatter.step({
  "line": 38,
  "name": "I should see Success Modal",
  "keyword": "Then "
});
formatter.step({
  "line": 39,
  "name": "I click on Close success modal",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "I should see Call Forwarding Status as \"No\"",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "https://accounts.amaysim.com.au/identity/login",
      "offset": 15
    }
  ],
  "location": "GenericSteps.i_navigate_to_something(String)"
});
formatter.result({
  "duration": 16139174754,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0468827174",
      "offset": 20
    }
  ],
  "location": "LoginPageSteps.i_type_username_as_something(String)"
});
formatter.result({
  "duration": 151124352,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "theHoff34",
      "offset": 20
    }
  ],
  "location": "LoginPageSteps.i_type_password_as_something(String)"
});
formatter.result({
  "duration": 135209621,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageSteps.i_click_on_login_button()"
});
formatter.result({
  "duration": 33241651430,
  "status": "passed"
});
formatter.match({
  "location": "DashboardsSteps.i_click_on_close_welcom_modal()"
});
formatter.result({
  "duration": 102246074,
  "status": "passed"
});
formatter.match({
  "location": "DashboardsSteps.i_should_be_at_dashboards()"
});
formatter.result({
  "duration": 99271664,
  "status": "passed"
});
formatter.match({
  "location": "DashboardsSteps.i_click_on_settings_in_main_menu()"
});
formatter.result({
  "duration": 78700317,
  "status": "passed"
});
formatter.match({
  "location": "GenericSteps.i_wait_site_to_load()"
});
formatter.result({
  "duration": 6498088314,
  "status": "passed"
});
formatter.match({
  "location": "SettingsPageSteps.i_should_be_at_settings_page()"
});
formatter.result({
  "duration": 120824111,
  "status": "passed"
});
formatter.match({
  "location": "SettingsPageSteps.i_click_on_edit_call_forwarding()"
});
formatter.result({
  "duration": 64110183,
  "status": "passed"
});
formatter.match({
  "location": "GenericSteps.i_wait_site_to_load()"
});
formatter.result({
  "duration": 1065576541,
  "status": "passed"
});
formatter.match({
  "location": "GenericSteps.i_click_on_confirm_button_modal()"
});
formatter.result({
  "duration": 2278931303,
  "status": "passed"
});
formatter.match({
  "location": "SettingsPageSteps.i_click_on_no_radio_button_call_forwarding()"
});
formatter.result({
  "duration": 100362772,
  "status": "passed"
});
formatter.match({
  "location": "GenericSteps.i_click_on_submit_button()"
});
formatter.result({
  "duration": 70429763,
  "status": "passed"
});
formatter.match({
  "location": "GenericSteps.i_should_see_success_modal()"
});
formatter.result({
  "duration": 5626354224,
  "status": "passed"
});
formatter.match({
  "location": "GenericSteps.i_click_on_close_success_modal()"
});
formatter.result({
  "duration": 55346032,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "No",
      "offset": 40
    }
  ],
  "location": "SettingsPageSteps.i_should_see_call_forwarding_status_as_something(String)"
});
formatter.result({
  "duration": 154569267,
  "status": "passed"
});
formatter.write("Current Page URL is https://www.amaysim.com.au/my-account/my-amaysim/settings");
formatter.embedding("image/png", "e740945f-ab8b-4754-af8a-8d277f9ad5e2.png");
formatter.scenario({
  "line": 43,
  "name": "TC03 - Setup Call Forwarding",
  "description": "",
  "id": "us01---settings;tc03---setup-call-forwarding",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 42,
      "name": "@US01_TC03"
    }
  ]
});
formatter.step({
  "line": 45,
  "name": "I navigate to \"https://accounts.amaysim.com.au/identity/login\"",
  "keyword": "Given "
});
formatter.step({
  "line": 46,
  "name": "I type username as \"0468827174\"",
  "keyword": "When "
});
formatter.step({
  "line": 47,
  "name": "I type password as \"theHoff34\"",
  "keyword": "When "
});
formatter.step({
  "line": 48,
  "name": "I click on Login button",
  "keyword": "When "
});
formatter.step({
  "line": 49,
  "name": "I click on Close welcome modal",
  "keyword": "When "
});
formatter.step({
  "line": 50,
  "name": "I should be at Dashboards",
  "keyword": "Then "
});
formatter.step({
  "line": 51,
  "name": "I click on Settings in main menu",
  "keyword": "When "
});
formatter.step({
  "line": 52,
  "name": "I wait site to load",
  "keyword": "When "
});
formatter.step({
  "line": 53,
  "name": "I should be at Settings page",
  "keyword": "Then "
});
formatter.step({
  "line": 54,
  "name": "I click on Edit Call forwarding",
  "keyword": "When "
});
formatter.step({
  "line": 55,
  "name": "I wait site to load",
  "keyword": "When "
});
formatter.step({
  "line": 56,
  "name": "I click on Confirm button Modal",
  "keyword": "When "
});
formatter.step({
  "line": 57,
  "name": "I click on Yes radio button Call forwarding",
  "keyword": "When "
});
formatter.step({
  "line": 58,
  "name": "I type Forward Call Number as \"0412345678\"",
  "keyword": "When "
});
formatter.step({
  "line": 59,
  "name": "I click on Submit button",
  "keyword": "When "
});
formatter.step({
  "line": 60,
  "name": "I should see Success Modal",
  "keyword": "Then "
});
formatter.step({
  "line": 61,
  "name": "I click on Close success modal",
  "keyword": "When "
});
formatter.step({
  "line": 62,
  "name": "I should see Call Forwarding Status as \"Yes\"",
  "keyword": "Then "
});
formatter.step({
  "line": 63,
  "name": "I should see Call Forwarding Number as \"0412345678\"",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "https://accounts.amaysim.com.au/identity/login",
      "offset": 15
    }
  ],
  "location": "GenericSteps.i_navigate_to_something(String)"
});
formatter.result({
  "duration": 8595594384,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0468827174",
      "offset": 20
    }
  ],
  "location": "LoginPageSteps.i_type_username_as_something(String)"
});
formatter.result({
  "duration": 167535245,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "theHoff34",
      "offset": 20
    }
  ],
  "location": "LoginPageSteps.i_type_password_as_something(String)"
});
formatter.result({
  "duration": 139306893,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageSteps.i_click_on_login_button()"
});
formatter.result({
  "duration": 27121316425,
  "status": "passed"
});
formatter.match({
  "location": "DashboardsSteps.i_click_on_close_welcom_modal()"
});
formatter.result({
  "duration": 79289169,
  "status": "passed"
});
formatter.match({
  "location": "DashboardsSteps.i_should_be_at_dashboards()"
});
formatter.result({
  "duration": 121339838,
  "status": "passed"
});
formatter.match({
  "location": "DashboardsSteps.i_click_on_settings_in_main_menu()"
});
formatter.result({
  "duration": 70629254,
  "status": "passed"
});
formatter.match({
  "location": "GenericSteps.i_wait_site_to_load()"
});
formatter.result({
  "duration": 5967869698,
  "status": "passed"
});
formatter.match({
  "location": "SettingsPageSteps.i_should_be_at_settings_page()"
});
formatter.result({
  "duration": 108957260,
  "status": "passed"
});
formatter.match({
  "location": "SettingsPageSteps.i_click_on_edit_call_forwarding()"
});
formatter.result({
  "duration": 61886913,
  "status": "passed"
});
formatter.match({
  "location": "GenericSteps.i_wait_site_to_load()"
});
formatter.result({
  "duration": 557619176,
  "status": "passed"
});
formatter.match({
  "location": "GenericSteps.i_click_on_confirm_button_modal()"
});
formatter.result({
  "duration": 2871049662,
  "status": "passed"
});
formatter.match({
  "location": "SettingsPageSteps.i_click_on_yes_radio_button_call_forwarding()"
});
formatter.result({
  "duration": 97384835,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0412345678",
      "offset": 31
    }
  ],
  "location": "SettingsPageSteps.i_type_forward_call_number_as_something(String)"
});
formatter.result({
  "duration": 245511684,
  "status": "passed"
});
formatter.match({
  "location": "GenericSteps.i_click_on_submit_button()"
});
formatter.result({
  "duration": 71256273,
  "status": "passed"
});
formatter.match({
  "location": "GenericSteps.i_should_see_success_modal()"
});
formatter.result({
  "duration": 5582264702,
  "status": "passed"
});
formatter.match({
  "location": "GenericSteps.i_click_on_close_success_modal()"
});
formatter.result({
  "duration": 48295198,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Yes",
      "offset": 40
    }
  ],
  "location": "SettingsPageSteps.i_should_see_call_forwarding_status_as_something(String)"
});
formatter.result({
  "duration": 146072281,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0412345678",
      "offset": 40
    }
  ],
  "location": "SettingsPageSteps.i_should_see_call_forwarding_number_as_something(String)"
});
formatter.result({
  "duration": 70111923,
  "status": "passed"
});
formatter.write("Current Page URL is https://www.amaysim.com.au/my-account/my-amaysim/settings");
formatter.embedding("image/png", "30358e4e-fd76-4bad-8d97-67e929a68582.png");
});