package cucumber.jvm.parallel.pageobjects;

public class DashboardsElements {

	public final String LOGOHEADER_CSS = "header.new-navigation svg.logo_new_navigation_designs";
	public final String LOGOUT_BUTTON_CSS = "a.logout-link";
	public final String CLOSE_WELCOMEMODAL_CSS = "div[id='welcome_popup'] a.close-reveal-modal";
	public final String SETTINGS_MAINMENU_CSS = "a[href$='/settings']";
}
