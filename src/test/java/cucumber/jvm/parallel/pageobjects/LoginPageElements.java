package cucumber.jvm.parallel.pageobjects;

public class LoginPageElements {

	public final String PAGE_TITLE = "My amaysim";
	public final String USERNAME_TEXTBOX_CSS = "input[id='username']";
	public final String PASSWORD_TEXTBOX_CSS = "input[id='password']";
	public final String LOGIN_BUTTON_CSS = "input[type='submit']";
	public final String FORGOTPASSWORD_LINK_CSS = "a.form-link";
}
