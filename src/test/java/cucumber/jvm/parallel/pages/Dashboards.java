package cucumber.jvm.parallel.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import cucumber.jvm.parallel.pageobjects.DashboardsElements;
import cucumber.jvm.parallel.pageobjects.LoginPageElements;
import cucumber.jvm.parallel.util.CUtil;

public class Dashboards {
    
	private static WebDriver webDriver;
    private DashboardsElements dashboardsElements;
    
    public Dashboards(WebDriver webDriver) {
        this.webDriver = webDriver;
        dashboardsElements = new DashboardsElements();
    }
    
    public boolean isAtThisPage(){
    	return CUtil.isDisplayed(By.cssSelector(dashboardsElements.LOGOHEADER_CSS));
    }
    
    public Dashboards click_LOGOUT_BUTTON(){
    	CUtil.click(By.cssSelector(dashboardsElements.LOGOUT_BUTTON_CSS));
    	return this;
    }
    
    public Dashboards click_CLOSE_WELCOMEMODAL(){
    	CUtil.click(By.cssSelector(dashboardsElements.CLOSE_WELCOMEMODAL_CSS));
    	return this;
    }
    
    public Dashboards click_SETTINGS_MAINMENU(){
    	CUtil.click(By.cssSelector(dashboardsElements.SETTINGS_MAINMENU_CSS));
    	return this;
    }
    
}
