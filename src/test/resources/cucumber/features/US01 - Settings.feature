@US01 
Feature: US01 - Settings

@US01_TC01 
Scenario: TC01 - Edit SIM Nickname
	
	Given I navigate to "https://accounts.amaysim.com.au/identity/login"
	When I type username as "0468827174"
	When I type password as "theHoff34"
	When I click on Login button
	When I click on Close welcome modal
	Then I should be at Dashboards
	When I click on Settings in main menu
	When I wait site to load
	Then I should be at Settings page
	When I click on Edit Sim Nickname
	When I type SIM Nickname as "Test 008"
	When I click on Submit button
	Then I should see SIM Nickname as "Test 008"
	
@US01_TC02 
Scenario: TC02 - Disable Call forwarding 
	
	Given I navigate to "https://accounts.amaysim.com.au/identity/login"
	When I type username as "0468827174"
	When I type password as "theHoff34"
	When I click on Login button
	When I click on Close welcome modal
	Then I should be at Dashboards
	When I click on Settings in main menu
	When I wait site to load
	Then I should be at Settings page
	When I click on Edit Call forwarding
	When I wait site to load
	When I click on Confirm button Modal
	When I click on No radio button Call forwarding
	When I click on Submit button
	Then I should see Success Modal
	When I click on Close success modal
	Then I should see Call Forwarding Status as "No"
	
@US01_TC03 
Scenario: TC03 - Setup Call Forwarding
	
	Given I navigate to "https://accounts.amaysim.com.au/identity/login"
	When I type username as "0468827174"
	When I type password as "theHoff34"
	When I click on Login button
	When I click on Close welcome modal
	Then I should be at Dashboards
	When I click on Settings in main menu
	When I wait site to load
	Then I should be at Settings page
	When I click on Edit Call forwarding
	When I wait site to load
	When I click on Confirm button Modal
	When I click on Yes radio button Call forwarding
	When I type Forward Call Number as "0412345678"
	When I click on Submit button
	Then I should see Success Modal
	When I click on Close success modal
	Then I should see Call Forwarding Status as "Yes"
	Then I should see Call Forwarding Number as "0412345678"
