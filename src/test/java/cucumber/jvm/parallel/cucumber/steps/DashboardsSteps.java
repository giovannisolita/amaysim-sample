package cucumber.jvm.parallel.cucumber.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.jvm.parallel.cucumber.Driver;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import cucumber.jvm.parallel.pages.Dashboards;

public class DashboardsSteps {
    private Dashboards dashboards;
    private WebDriver webDriver;
    
    public DashboardsSteps(Driver webDriver) {
        this.webDriver = webDriver;
        dashboards = new Dashboards(webDriver);
    }
    
    @Then("^I should be at Dashboards$")
    public void i_should_be_at_dashboards() throws Throwable {
    	Assert.assertTrue(dashboards.isAtThisPage());
    }
    
    @When("^I click on Logout button$")
    public void i_click_on_logout_button() throws Throwable {
    	dashboards.click_LOGOUT_BUTTON();
    }
    
    @When("^I click on Close welcome modal$")
    public void i_click_on_close_welcom_modal() throws Throwable {
    	dashboards.click_CLOSE_WELCOMEMODAL();
    }
    
    @When("^I click on Settings in main menu$")
    public void i_click_on_settings_in_main_menu() throws Throwable {
    	dashboards.click_SETTINGS_MAINMENU();
    }
    
}
