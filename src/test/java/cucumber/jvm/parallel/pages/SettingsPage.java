package cucumber.jvm.parallel.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import cucumber.jvm.parallel.pageobjects.SettingsPageElements;
import cucumber.jvm.parallel.util.CUtil;

public class SettingsPage {
    
	private static WebDriver webDriver;
    private SettingsPageElements settingsPageElements;
    
    public SettingsPage(WebDriver webDriver) {
        this.webDriver = webDriver;
        settingsPageElements = new SettingsPageElements();
    }
    
    public boolean isAtThisPage(){
    	return CUtil.isDisplayed(By.cssSelector(settingsPageElements.HEADER_CSS))
    			&& CUtil.getText(By.cssSelector(settingsPageElements.HEADER_CSS)).contains("settings");
    }
    
    public SettingsPage click_EDIT_SIMNICKNAME(){
    	CUtil.click(By.cssSelector(settingsPageElements.EDIT_SIMNICKNAME_CSS));
    	return this;
    }
    
    public SettingsPage type_SIMNICKNAME_TEXTBOX(String text){
    	CUtil.type(By.cssSelector(settingsPageElements.SIMNICKNAME_TEXTBOX_CSS), text);
    	return this;
    }

	public boolean isEqual_SIMNICKNAME_LABEL(String text) {
		return CUtil.getText(By.cssSelector(settingsPageElements.SIMNICKNAME_LABEL_CSS)).equals(text);
	}
	
	public SettingsPage click_NO_CALLFORWARDING_RADIO(){
		CUtil.click(By.xpath(settingsPageElements.NO_CALLFORWARDING_RADIO_XPATH));
		return this;
	}
	
	public SettingsPage click_YES_CALLFORWARDING_RADIO(){
		CUtil.click(By.xpath(settingsPageElements.YES_CALLFORWARDING_RADIO_XPATH));
		return this;
	}
	
	public SettingsPage type_FORWARDCALLNUMBER_TEXTBOX(String text){
		CUtil.type(By.cssSelector(settingsPageElements.FORWARDCALLNUMBER_TEXTBOX_CSS), text);
		return this;
	}
	
	public SettingsPage click_EDIT_CALLFORWARDING(){
		CUtil.click(By.cssSelector(settingsPageElements.EDIT_CALLFORWARDING_CSS));
		return this;
	}
	
	public boolean isEqual_STATUS_CALLFORWARDING(String text){
		System.out.println(CUtil.getText(By.cssSelector(settingsPageElements.STATUS_CALLFORWARDING_CSS)));
		System.out.println(text);
		return CUtil.getText(By.cssSelector(settingsPageElements.STATUS_CALLFORWARDING_CSS)).equals(text);
	}
	
	public boolean isContains_CALLFORWARDING_NUMBER_LABEL(String text){
		return CUtil.isContainsText(By.cssSelector(settingsPageElements.CALLFORWARDING_NUMBER_LABEL), text);
	}
    
}
