package cucumber.jvm.parallel.pageobjects;

public class GenericElements {
	public final String SUBMIT_BUTTON_CSS = "input[type='submit']";
	public final String CONFIRM_BUTTON_MODAL_CSS = "div.form_confirm_popup a.confirm_popup_confirm";
	public final String SUCCESS_MODAL_CSS = "div[data-popup-type='success']";
	public final String CLOSE_BUTTON_SUCCESSMODAL_CSS = "div[data-popup-type='success'] a[class='close-reveal-modal']";
	public final String MODALBLOCK_CSS = "div.reveal-modal-bg";
	public final String AJAXLOADING_CSS = "div[id='ajax_loading']";
}
