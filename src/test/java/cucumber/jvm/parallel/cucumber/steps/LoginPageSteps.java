package cucumber.jvm.parallel.cucumber.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.jvm.parallel.cucumber.Driver;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import cucumber.jvm.parallel.pages.LoginPage;

public class LoginPageSteps {
    private LoginPage loginPage;
    private WebDriver webDriver;
    
    public LoginPageSteps(Driver webDriver) {
        this.webDriver = webDriver;
        loginPage = new LoginPage(webDriver);
    }
    
    @When("^I type username as \"([^\"]*)\"$")
    public void i_type_username_as_something(String text) throws Throwable {
    	loginPage.type_USERNAME_TEXTBOX(text);
    }

    @When("^I type password as \"([^\"]*)\"$")
    public void i_type_password_as_something(String text) throws Throwable {
    	loginPage.type_PASSWORD_TEXTBOX(text);
    }

    @When("^I click on Login button$")
    public void i_click_on_login_button() throws Throwable {
    	loginPage.click_LOGIN_BUTTON();
    }
    
    @Then("^I should be at the Login page$")
    public void i_should_be_at_the_login_page() throws Throwable {
    	Assert.assertTrue(loginPage.isAtThisPage());
    }
	
}
