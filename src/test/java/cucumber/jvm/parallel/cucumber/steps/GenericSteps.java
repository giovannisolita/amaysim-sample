package cucumber.jvm.parallel.cucumber.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.jvm.parallel.cucumber.Driver;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import cucumber.jvm.parallel.pages.Generic;

public class GenericSteps {
    private Generic generic;
    private WebDriver webDriver;
    
    public GenericSteps(Driver webDriver) {
        this.webDriver = webDriver;
        generic = new Generic(webDriver);
    }
    
    @Given("^I navigate to \"([^\"]*)\"$")
    public void i_navigate_to_something(String url) throws Throwable {
    	generic.navigateTo(url);
    }
    
    @When("^I click on Submit button$")
    public void i_click_on_submit_button() throws Throwable {
    	generic.click_SUBMIT_BUTTON();
    }
    
    @When("^I click on Confirm button Modal$")
    public void i_click_on_confirm_button_modal() throws Throwable {
    	generic.click_CONFIRM_BUTTON_MODAL();
    }
    
    @Then("^I should see Success Modal$")
    public void i_should_see_success_modal() throws Throwable {
        Assert.assertTrue(generic.isDisplayed_SUCCESS_MODAL());
    }
    
    @When("^I click on Close success modal$")
    public void i_click_on_close_success_modal() throws Throwable {
    	generic.click_CLOSE_BUTTON_SUCCESSMODAL();
    }
    
    @When("^I wait site to load$")
    public void i_wait_site_to_load() throws Throwable {
    	generic.waitToLoad();
    }
    
}
