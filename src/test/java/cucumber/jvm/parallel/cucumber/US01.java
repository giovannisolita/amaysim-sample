package cucumber.jvm.parallel.cucumber;

import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@Cucumber.Options(
		features = "src/test/resources/cucumber/features/",
		tags = { "@US01" }, 
		format = {"html:target/cucumber-report/US01" })
public class US01 {
}
