package cucumber.jvm.parallel.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;

public class CUtil {
	private static WebDriver webDriver;

	private static boolean HIGHLIGHT_SWITCH = true;

	private final static int WAITING_TIME = 60;

	public static String storedValue = "";
	
	public static void setWebDriver(WebDriver _webDriver){
		webDriver = _webDriver;
	}

	public static void switchFrame(String idOrName) {
		webDriver.switchTo().frame(idOrName);
	}

	public static void switchFrameByIndex(int index) {
		webDriver.switchTo().frame(index);
	}

	public static void switchToTopFrame() {
		webDriver.switchTo().defaultContent();
	}

	public static boolean javascript_highlight(By by, String borderColor, String borderType, int borderThickness) {
		try {
			WebElement pageElement = webDriver.findElement(by);
			if (HIGHLIGHT_SWITCH) {
				if (borderColor.isEmpty()) {
					borderColor = "red";
				}
				if (borderType.isEmpty()) {
					borderType = "solid";
				}
				if (webDriver instanceof JavascriptExecutor) {
					((JavascriptExecutor) webDriver).executeScript("arguments[0].style.border='" + borderThickness
							+ "px " + borderType + " " + borderColor + "'", pageElement);
				}
			}
			return true;
		} catch (ElementNotVisibleException | NoSuchElementException | StaleElementReferenceException e) {
			return false;
		}
	}

	public static boolean javascript_highlight(WebElement pageElement, String borderColor, String borderType,
			int borderThickness) {
		try {
			if (HIGHLIGHT_SWITCH) {
				if (borderColor.isEmpty()) {
					borderColor = "red";
				}
				if (borderType.isEmpty()) {
					borderType = "solid";
				}
				if (webDriver instanceof JavascriptExecutor) {
					((JavascriptExecutor) webDriver).executeScript("arguments[0].style.border='" + borderThickness
							+ "px " + borderType + " " + borderColor + "'", pageElement);
				}
			}
			return true;
		} catch (ElementNotVisibleException | NoSuchElementException | StaleElementReferenceException e) {
			return false;
		}
	}

	public static void waitForElement(final By by) {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(webDriver).withTimeout(WAITING_TIME, TimeUnit.SECONDS)
				.pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class)
				.ignoring(ElementNotVisibleException.class).ignoring(TimeoutException.class)
				.ignoring(StaleElementReferenceException.class);
		WebElement foo = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				return driver.findElement(by);
			}
		});
	}

	public static void waitForVisibleElement(final By by) {
		WebDriverWait wait = new WebDriverWait(webDriver, WAITING_TIME);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	public static void waitForClickableElement(final By by) {
		WebDriverWait wait = new WebDriverWait(webDriver, WAITING_TIME);
		wait.until(ExpectedConditions.elementToBeClickable(by));
	}

	public static void waitForElementDisappear(final By by) {
		WebDriverWait wait = new WebDriverWait(webDriver, WAITING_TIME);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
	}

	public static boolean waitUtilAtNextPage(String titlePage) {
		try {
			WebDriverWait wait = new WebDriverWait(webDriver, WAITING_TIME);
			wait.ignoring(TimeoutException.class).until(ExpectedConditions.titleContains(titlePage));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}

	public static void type(By by, String text) {
		waitForElement(by);
		do {
			try {
				javascript_highlight(by, "blue", "dotted", 3);
				webDriver.findElement(by).clear();
				webDriver.findElement(by).sendKeys(text);
				break;
			} catch (StaleElementReferenceException e) {

			}
		} while (true);
	}

	public static void type(WebElement pageElement, String text) {
		do {
			try {
				pageElement.clear();
				pageElement.sendKeys(text);
				break;
			} catch (InvalidElementStateException e) {

			}
		} while (true);
	}

	public static void click(By by) {
		waitForElement(by);
		javascript_highlight(by, "blue", "dotted", 3);
		javaClick(by);
	}

	public static void doubleClick(By by) {
		waitForElement(by);
		javascript_highlight(by, "blue", "dotted", 3);
		javaDoubleClick(by);
	}

	public static void javaScrollIntoView(By by) {
		JavascriptExecutor executor = (JavascriptExecutor) webDriver;
		executor.executeScript("arguments[0].scrollIntoView();", webDriver.findElement(by));
	}

	public static void javaClick(By by) {
		JavascriptExecutor executor = (JavascriptExecutor) webDriver;
		executor.executeScript("arguments[0].click();", webDriver.findElement(by));
	}

	public static void javaDoubleClick(By by) {
		JavascriptExecutor executor = (JavascriptExecutor) webDriver;
		executor.executeScript(
				"var event = new MouseEvent('dblclick', { 'view': window,  'bubbles': true,    'cancelable': true  }); arguments[0].dispatchEvent(event)",
				webDriver.findElement(by));
	}

	public static void javaClick(WebElement pageElement) {
		JavascriptExecutor executor = (JavascriptExecutor) webDriver;
		executor.executeScript("arguments[0].click();", pageElement);
	}
	
	public static void actionClick(By by){
		Actions actions = new Actions(webDriver);
		actions.moveToElement(webDriver.findElement(by)).click();
		actions.perform();
	}
	
	public static void actionClick(WebElement element){
		Actions actions = new Actions(webDriver);
		actions.moveToElement(element).click();
		actions.perform();
	}

	public static boolean isDisplayed(By by) {
		try {
			waitForVisibleElement(by);
			javascript_highlight(by, "green", "dotted", 3);
			boolean diplayed = webDriver.findElement(by).isDisplayed();
			return diplayed;
		} catch (NoSuchElementException | TimeoutException e) {
			return false;
		}
	}

	public static void pickDate(WebElement pageElement, String date) {
		javaClick(pageElement);
		pickDate(date);
	}

	public static void pickDate(By by, String date) {
		click(by);
		pickDate(date);
	}

	private static void pickDate(String date) {
		// MM/DD/YYYY format
		String month = date.split("/")[0];
		String day = date.split("/")[1];
		String year = date.split("/")[2];
		switch (month) {
		case "01":
			month = "January";
			break;
		case "02":
			month = "February";
			break;
		case "03":
			month = "March";
			break;
		case "04":
			month = "April";
			break;
		case "05":
			month = "May";
			break;
		case "06":
			month = "June";
			break;
		case "07":
			month = "July";
			break;
		case "08":
			month = "August";
			break;
		case "09":
			month = "September";
			break;
		case "10":
			month = "October";
			break;
		case "11":
			month = "November";
			break;
		case "12":
			month = "December";
			break;
		}

		for (int occur = 0; occur < 3; occur++) {
			CUtil.click(By.cssSelector("button.uib-title"));
		}
		
		CUtil.javaClick(By.xpath("//button/span[text()='"+year+"']"));
		CUtil.javaClick(By.xpath("//button/span[text()='"+month+"']"));
		CUtil.javaClick(By.xpath("//button/span[text()='"+day+"' and not(contains(@class,'muted'))]"));

		/*
		for (WebElement yearElement : webDriver.findElements(By.cssSelector("button.btn span"))) {
			try {
				if (yearElement.getText().equals(year)) {
					CUtil.javaClick(yearElement);
				}
			} catch (StaleElementReferenceException e) {

			}
		}

		for (WebElement monthElement : webDriver.findElements(By.cssSelector("button.btn span"))) {
			try {
				if (monthElement.getText().equals(month)) {
					CUtil.javaClick(monthElement);
				}
			} catch (StaleElementReferenceException e) {

			}
		}

		for (WebElement dayElement : webDriver
				.findElements(By.cssSelector("button.btn span:not([class*='muted'])"))) {
			try {
				if (dayElement.getText().equals(day)) {
					CUtil.javaClick(dayElement);
				}
			} catch (StaleElementReferenceException e) {

			}
		}
		*/
	}

	public static boolean isContainsText(By by, String text) {
		javascript_highlight(by, "green", "dotted", 3);
		return webDriver.findElement(by).getText().contains(text);
	}

	public static boolean isContainsValue(By by, String text) {
		javascript_highlight(by, "green", "dotted", 3);
		return webDriver.findElement(by).getAttribute("value").contains(text);
	}

	public static boolean isEqualText(By by, String text) {
		javascript_highlight(by, "green", "dotted", 3);
		return webDriver.findElement(by).getText().equals(text);
	}

	public static boolean isEqualValue(By by, String text) {
		javascript_highlight(by, "green", "dotted", 3);
		return webDriver.findElement(by).getAttribute("value").equals(text);
	}

	public static boolean isChecked(By by, boolean expectedStatus) {
		javascript_highlight(by, "green", "dotted", 3);
		if (webDriver.findElement(by).isSelected() && expectedStatus)
			return true;
		else
			return false;
	}

	public static void selectDropdown_CSS(String locator, String text) {
		Assert.assertTrue("Option '" + text + "' does not exist in dropdown!",
				selectDropdown_checkMatch(locator, text));
	}

	public static boolean selectDropdown_checkMatch(String locator, String text) {
		waitForElement(By.cssSelector(locator + " span.ui-select-toggle"));
		List<WebElement> pageElements = webDriver.findElements(By.cssSelector(locator + " span.ui-select-toggle"));
		WebElement pageElement = pageElements.get(pageElements.size() - 1);
		javascript_highlight(pageElement, "blue", "dotted", 3);
		javaClick(pageElement);
		waitForElement(By.cssSelector(locator + " ul.ui-select-dropdown"));
		waitForElement(By.cssSelector("a.ui-select-choices-row-inner"));
		waitForVisibleElement(By.cssSelector("a.ui-select-choices-row-inner"));
		List<WebElement> webElements;
		boolean again = true;
		while (again) {
			webElements = webDriver.findElements(By.cssSelector("a.ui-select-choices-row-inner"));
			for (WebElement webElement : webElements) {
				try {
					if (webElement.getText().contains(text)) {
						javascript_highlight(webElement, "blue", "dotted", 3);
						javaClick(webElement);
						return true;
					}
				} catch (StaleElementReferenceException e) {
					again = true;
				}
				again = false;
			}
		}
		return false;
	}

	public static void click_CSS(String locator) {
		Assert.assertTrue("Clickable with locator '" + locator + "' does not exist!", click_checkMatch(locator));
	}

	public static boolean click_checkMatch(String locator) {
		try {
			waitForElement(By.cssSelector(locator));
			List<WebElement> pageElements = webDriver.findElements(By.cssSelector(locator));
			WebElement pageElement = pageElements.get(pageElements.size() - 1);
			javascript_highlight(pageElement, "blue", "dotted", 3);
			javaClick(pageElement);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public static void type_CSS(String locator, String text) {
		waitForElement(By.cssSelector(locator));
		List<WebElement> pageElements = webDriver.findElements(By.cssSelector(locator));
		WebElement pageElement = pageElements.get(pageElements.size() - 1);
		javascript_highlight(pageElement, "blue", "dotted", 3);
		pageElement.sendKeys(text);
	}

	public static void check_CSS(String locator, boolean intendedStatus) {
		waitForElement(By.cssSelector(locator + " span.icon-check"));
		List<WebElement> pageElements = webDriver.findElements(By.cssSelector(locator + " span.icon-check"));
		WebElement pageElement = pageElements.get(pageElements.size() - 1);
		javascript_highlight(pageElement, "blue", "dotted", 3);
		if (!CUtil.isDisplayed(By.cssSelector(locator + " span[aria-hidden='true']"))) {
			javaClick(pageElement);
		}
	}

	public static void recordText(By by) {
		javascript_highlight(by, "green", "dotted", 3);
		storedValue = webDriver.findElement(by).getText();
	}

	public static boolean isAlphabeticalDropdown_CSS(String locator) {
		waitForElement(By.cssSelector(locator + " span.ui-select-toggle"));
		List<WebElement> pageElements = webDriver.findElements(By.cssSelector(locator + " span.ui-select-toggle"));
		WebElement pageElement = pageElements.get(pageElements.size() - 1);
		javascript_highlight(pageElement, "blue", "dotted", 3);
		javaClick(pageElement);
		waitForElement(By.cssSelector(locator + " ul.ui-select-dropdown"));
		String previous = "";
		List<String> theList = new ArrayList<>();
		for (WebElement webElement : webDriver.findElements(By.cssSelector("a.ui-select-choices-row-inner"))) {
			try {
				if (webElement.getText().equalsIgnoreCase("Not Applicable"))
					theList.add(webElement.getText());
			} catch (StaleElementReferenceException e) {
				// ignore useless StaleElementReferenceException
			}
		}

		for (final String current : theList) {
			if (current.compareTo(previous) < 0) {
				System.out.println(current + " < " + previous);
				return false;
			}
			previous = current;
		}

		return true;
	}

	public static String getAttribute(By by, String text) {
		return webDriver.findElement(by).getAttribute(text);
	}

	public static String getText(By by) {
		waitForVisibleElement(by);
		return webDriver.findElement(by).getText();
	}

	public static boolean check_ORDER(ArrayList<String> arr, String locator) {

		for (int i = 0; i < arr.size(); i++) {
			if (!arr.get(i).equals(webDriver.findElements(By.xpath(locator)).get(i).getText().trim())) {

				return false;
			}
		}
		return true;
	}
	
	public static boolean isDisplayedAlert(){
	    try 
	    { 
	        webDriver.switchTo().alert(); 
	        return true; 
	    }   
	    catch (NoAlertPresentException Ex) 
	    { 
	        return false; 
	    }    
	}
	
	public static String getProperty(String location, String property){
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream(location);

			// load a properties file
			prop.load(input);

			return prop.getProperty(property);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return "";
	}

}