package cucumber.jvm.parallel.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import cucumber.jvm.parallel.pageobjects.LoginPageElements;
import cucumber.jvm.parallel.util.CUtil;

public class LoginPage {
    
	private static WebDriver webDriver;
    private LoginPageElements loginPageElements;
    
    public LoginPage(WebDriver webDriver) {
        this.webDriver = webDriver;
        loginPageElements = new LoginPageElements();
    }
    
    public boolean isAtThisPage(){
    	return CUtil.waitUtilAtNextPage(loginPageElements.PAGE_TITLE);
    }
    
    public LoginPage type_USERNAME_TEXTBOX(String text){
    	CUtil.type(By.cssSelector(loginPageElements.USERNAME_TEXTBOX_CSS), text);
    	return this;
    }
    
    public LoginPage type_PASSWORD_TEXTBOX(String text){
    	CUtil.type(By.cssSelector(loginPageElements.PASSWORD_TEXTBOX_CSS), text);
    	return this;
    }
    
    public LoginPage click_LOGIN_BUTTON(){
    	CUtil.click(By.cssSelector(loginPageElements.LOGIN_BUTTON_CSS));
    	return this;
    }
    
}
