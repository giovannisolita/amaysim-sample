package cucumber.jvm.parallel.pageobjects;

public class SettingsPageElements {
	public final String HEADER_CSS = "div.ama-page-header";
	
	public final String EDIT_SIMNICKNAME_CSS = "a[id='edit_settings_phone_label']";
	public final String SIMNICKNAME_TEXTBOX_CSS = "input[id='my_amaysim2_setting_phone_label']";
	public final String SIMNICKNAME_LABEL_CSS = "div[id='settings_sim_nickname'] div.setting-option-details-text";
	
	public final String EDIT_CALLFORWARDING_CSS = "a[id='edit_settings_call_forwarding']";
	public final String NO_CALLFORWARDING_RADIO_CSS = "input[id='my_amaysim2_setting_call_divert_false']";
	public final String YES_CALLFORWARDING_RADIO_CSS = "input[id='my_amaysim2_setting_call_divert_true']";
	public final String NO_CALLFORWARDING_RADIO_XPATH = "//input[@id='my_amaysim2_setting_call_divert_false']/parent::label";
	public final String YES_CALLFORWARDING_RADIO_XPATH = "//input[@id='my_amaysim2_setting_call_divert_true']/parent::label";
	public final String FORWARDCALLNUMBER_TEXTBOX_CSS = "input[id='my_amaysim2_setting_call_divert_number']";
	public final String STATUS_CALLFORWARDING_CSS = "div[id='settings_call_forwarding'] div.setting-option-value-text";
	public final String CALLFORWARDING_NUMBER_LABEL = "div[id='settings_call_forwarding'] div.setting-option-details-text";
}
