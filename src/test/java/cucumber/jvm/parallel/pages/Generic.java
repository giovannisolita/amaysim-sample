package cucumber.jvm.parallel.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import cucumber.jvm.parallel.pageobjects.GenericElements;
import cucumber.jvm.parallel.pageobjects.LoginPageElements;
import cucumber.jvm.parallel.util.CUtil;

public class Generic {
    
	private static WebDriver webDriver;
    private GenericElements genericElements;
    
    public Generic(WebDriver webDriver) {
        this.webDriver = webDriver;
        genericElements = new GenericElements();
    }
    
    public Generic navigateTo(String url){
    	webDriver.get(url);
    	webDriver.manage().window().maximize();
    	return this;
    }
    
    public Generic click_SUBMIT_BUTTON(){
    	CUtil.click(By.cssSelector(genericElements.SUBMIT_BUTTON_CSS));
    	return this;
    }
    
    public Generic click_CONFIRM_BUTTON_MODAL(){
    	CUtil.click(By.cssSelector(genericElements.CONFIRM_BUTTON_MODAL_CSS));
    	return this;
    }

	public boolean isDisplayed_SUCCESS_MODAL() {
		return CUtil.isDisplayed(By.cssSelector(genericElements.SUCCESS_MODAL_CSS));
	}
	
	public Generic click_CLOSE_BUTTON_SUCCESSMODAL(){
		CUtil.click(By.cssSelector(genericElements.CLOSE_BUTTON_SUCCESSMODAL_CSS));
		return this;
	}
	
	public Generic waitToLoad(){
		CUtil.waitForElementDisappear(By.cssSelector(genericElements.AJAXLOADING_CSS));
		return this;
	}
    
}
