package cucumber.jvm.parallel.cucumber;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.jvm.parallel.util.CUtil;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.concurrent.TimeUnit;

public class Driver extends EventFiringWebDriver {
    private static final WebDriver MAINDRIVER;
    private static final Thread CLOSE_THREAD = new Thread() {
        @Override
        public void run() {
        	MAINDRIVER.quit();
        }
    };

    static {
    	switch(System.getProperty("browser")){
    	case "firefox":
    	case "FIREFOX":
    		MAINDRIVER = new FirefoxDriver();
    			break;
    	case "chrome":
    	case "CHROME":
    		default:
    			System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver_Win.exe");
    			MAINDRIVER = new ChromeDriver();
    	        break;
    	}
    	MAINDRIVER.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        Runtime.getRuntime().addShutdownHook(CLOSE_THREAD);
    }

    public Driver() {
        super(MAINDRIVER);
        CUtil.setWebDriver(MAINDRIVER);
    }

    @Override
    public void close() {
        if (Thread.currentThread() != CLOSE_THREAD) {
            throw new UnsupportedOperationException("You shouldn't close this WebDriver. It's shared and will close when the JVM exits.");
        }
        super.close();
    }

    @Before
    public void deleteAllCookies() {
        manage().deleteAllCookies();
    }

    @After
    public void embedScreenshot(Scenario scenario) {
        scenario.write("Current Page URL is " + getCurrentUrl());
        try {
            byte[] screenshot = getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
        } catch (WebDriverException somePlatformsDontSupportScreenshots) {
            System.err.println(somePlatformsDontSupportScreenshots.getMessage());
        }
    }

}
