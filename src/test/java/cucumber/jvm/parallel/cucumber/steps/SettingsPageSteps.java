package cucumber.jvm.parallel.cucumber.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.jvm.parallel.cucumber.Driver;
import cucumber.jvm.parallel.pages.SettingsPage;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class SettingsPageSteps {
    private SettingsPage settingsPage;
    private WebDriver webDriver;
    
    public SettingsPageSteps(Driver webDriver) {
        this.webDriver = webDriver;
        settingsPage = new SettingsPage(webDriver);
    }
    
    @Then("^I should be at Settings page$")
    public void i_should_be_at_settings_page() throws Throwable {
    	Assert.assertTrue(settingsPage.isAtThisPage());
    }
    
    @When("^I click on Edit Sim Nickname$")
    public void i_click_on_edit_sim_nickname() throws Throwable {
    	settingsPage.click_EDIT_SIMNICKNAME();
    }
    
    @When("^I type SIM Nickname as \"([^\"]*)\"$")
    public void i_type_sim_nickname_as_something(String text) throws Throwable {
    	settingsPage.type_SIMNICKNAME_TEXTBOX(text);
    }
    
    @Then("^I should see SIM Nickname as \"([^\"]*)\"$")
    public void i_should_see_sim_nickname_as_something(String text) throws Throwable {
    	Assert.assertTrue(settingsPage.isEqual_SIMNICKNAME_LABEL(text));
    }
    
    @When("^I click on No radio button Call forwarding$")
    public void i_click_on_no_radio_button_call_forwarding() throws Throwable {
    	settingsPage.click_NO_CALLFORWARDING_RADIO();
    }
    
    @When("^I click on Yes radio button Call forwarding$")
    public void i_click_on_yes_radio_button_call_forwarding() throws Throwable {
    	settingsPage.click_YES_CALLFORWARDING_RADIO();
    }
    
    @When("^I click on Edit Call forwarding$")
    public void i_click_on_edit_call_forwarding() throws Throwable {
    	settingsPage.click_EDIT_CALLFORWARDING();
    }
    
    @Then("^I should see Call Forwarding Status as \"([^\"]*)\"$")
    public void i_should_see_call_forwarding_status_as_something(String text) throws Throwable {
    	Assert.assertTrue(settingsPage.isEqual_STATUS_CALLFORWARDING(text));
    }
    
    @When("^I type Forward Call Number as \"([^\"]*)\"$")
    public void i_type_forward_call_number_as_something(String text) throws Throwable {
    	settingsPage.type_FORWARDCALLNUMBER_TEXTBOX(text);
    }
    
    @Then("^I should see Call Forwarding Number as \"([^\"]*)\"$")
    public void i_should_see_call_forwarding_number_as_something(String text) throws Throwable {
        Assert.assertTrue(settingsPage.isContains_CALLFORWARDING_NUMBER_LABEL(text));
    }
    
}
